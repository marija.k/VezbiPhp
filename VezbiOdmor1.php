<!DOCTYPE html>
<html>
<head>
	<title>Vezbi za na odmor 1</title>

</head>
<body>
	<div style="color: lightblue">
		<?php
			function printInfo($ime,$data,$mob)
			{
				echo "Ime: $ime <br>Data na raganje: $data<br>Mobilen broj: $mob<br>";
			}
			function info($array)
			{
				foreach ($array as $key => $value) {
					echo "$key: $value <br>";
				}
			}

			printInfo('Marija','23-08-1995','078-332-865');
			$marija = array('Ime' => 'Marija' ,
							'Data_na_raganje'=> '23.08.1995' , 
							'Mobilen' => '078-332-865');

			info($marija);
			?>
	</div>
<hr>
<div style="color: blue">
	<?php
		function PaA ($h,$w)
		{
			$p=2*($h+$w);
			$a=$h*$w;
			return [$p,$a];
		}
		$rec1=PaA(7,5);
		echo "The perimeter is $rec1[0] and the area is: $rec1[1]<br>";
	?>
</div>
<hr>
<div style="color: lightgreen">
	<?php
		function ywd($days)
		{
			$niza=[0,0,0];
			$niza[0]=floor($days/365);
			$x=$days%365;
			$niza[1]=floor($x/7);
			$niza[2]=$x%7;
			return $niza;
		}
		$days=728;
		$niza=ywd($days);
		echo "Vo $days dena ima $niza[0] godini, $niza[1] nedeli i $niza[2] denovi <br/>";
	?>
</div>
<hr>
<div style="color: green">
	<?php
		function maxin($x,$y,$z)
		{
			if($x<$y)
			{
				if($y<$z)
				{
					return $z;
				}
				else
				{
					return $y;
				}
			}
			else
			{
				if($x<$z)
				{
					return $z;
				}
				else 
				{
					return $x;
				}
			}
		}
		function maxina($x,$y,$z)
		{
			$max=$x;
			$array=[$x,$y,$z];
			foreach ($array as $key => $value) {
				if($max<$value)
				{
					$max=$value;
				}
			}
			return $max;
		}

		echo maxin(1,2,3)."<br />";
		echo maxin(6,5,4)."<br />";
		echo maxin(7,9,8)."<br />";

		echo maxina(1,2,3)."<br />";
		echo maxina(6,5,4)."<br />";
		echo maxina(7,9,8)."<br />";
	?>
</div>
<hr>
<div style="color: darkblue">
	<?php
		function distance ($p1,$p2){
			$d=sqrt((($p2[0]-$p1[0])**2)+(($p2[1]-$p1[1]))**2);
			$x=$p2[0]-$p1[0];
			$x2=$x**2;
			$y=$p2[1]-$p1[1];
			$y2=$y**2;
			$xy=$x2+$y2;
			return $d;
		}

		$point1=[2,4];
		$point2=[2,6];
		echo distance($point1,$point2)."<br/>";
	?>
</div>
<hr>
<div style="color: pink">
	<?php
		function even ()
		{
			for ($i=1; $i <=50 ; $i++) { 
				if($i%2==0)
				{
					echo $i." ";
				}	
			}
		}
		even();
		echo "<br />";
	?>
</div>
<hr>
<div style="color: orange">
	<?php
		function toF($c)
		{
			return $c*(9/5)+32; 
		}
		$c=20;
		$f=toF($c);
		echo "$c Centigrade in Fahrenheit is: $f<br/>";
	?>
</div>
<hr>
<div style="color: purple">
	<?php
		function minutes($h,$m)
		{
			return ($h*60)+$m;
		}
		$m=minutes(2,15);
		echo "$m<br/>";

	?>
</div>
<hr>
<div style="color: lime">
	<?php
		function kTm($k)
		{
			return $k*0.6213711922;
		}
		$mph=kTm(80);
		echo "$mph ili priblizno ";
		settype($mph, "integer");
		echo "$mph mph<br/>";
	?>
</div>
<hr>
<div style="color: red">
	<?php
		function posOrNeg($x)
		{
			if($x<0)
			{
				echo "The number $x is negative.<br/>";
			}
			elseif ($x>0) {
				echo "The number $x is positive.<br/>";
			}
			else{
				echo "The number $x is zero.<br/>";
			}
		}
		posOrNeg(2);
		posOrNeg(-0.5);
		posOrNeg(0);
	?>
</div>
<hr>
<div style="color: yellow">
	<?php
		function vote($x)
		{
			if($x>=18)
			{
				echo "The candidate is eligble to vote. <br/>";
			}
			else
			{
				echo "The candidate is not eligble to vote. <br/>";
			}
		}

		function voteA($array)
		{
			if($array["age"]>=18)
			{
				echo "The candidate ".$array['name']." is eligble to vote. <br/>";
			}
			else
			{
				echo "The candidate ".$array['name']." is not eligble to vote. <br/>";
			}
		}

		$array = array('name' => 'Marija',
		 				'age'=> 22,
		 				'city' => 'Skopje');
		voteA($array);
		vote (18);
		vote (18);
		vote (54);

	?>
</div>
<hr>
<div style="color: gray">
	<?php
		function ads($x)
		{
			if(is_numeric($x))
			{
				echo "The character $x is digit. <br/>";
			}
			elseif (ctype_alpha($x)) {
				echo "The character $x is alphabetic. <br/>";
			}
			elseif(preg_match('/[^a-zA-Z\d]/', $x))
			{
				echo "The character $x is a special character. <br/>";
			}
		}

		ads('x');
		ads('X');
		ads(12);
		ads('+');
	?>
</div>
<hr>
<div style="color: brown">
	<?php
		function ntw ($array)
		{
			foreach ($array as $key => $value) {
				switch ($value) {
					case 1:
						echo "$value => One <br/>";
						break;
					case 2:
						echo "$value => Two <br/>";
						break;
					case 3:
						echo "$value => Three <br/>";
						break;
					case 4:
						echo "$value => Four <br/>";
						break;
					case 5:
						echo "$value => Five <br/>";
						break;
					case 6:
						echo "$value => Six <br/>";
						break;
					case 7:
						echo "$value => Seven <br/>";
						break;
					case 8:
						echo "$value => Eight <br/>";
						break;
					case 9:
						echo "$value => Nine <br/>";
						break;
					default:
						echo "Invalid input - only numbers 1-9.<br/>";
						break;
				}
			}
		}
		$niza=[1,3,6,9,0];
		ntw($niza);
	?>
</div>
<hr>
<div>
	<?php
		function numbToMonth ($x)
		{
			switch ($x) {
				case 1:
						echo "1 => January <br/>";
						break;
					case 2:
						echo "2 => February <br/>";
						break;
					case 3:
						echo "3 => March <br/>";
						break;
					case 4:
						echo "4 => April <br/>";
						break;
					case 5:
						echo "5 => May <br/>";
						break;
					case 6:
						echo "6 => June <br/>";
						break;
					case 7:
						echo "7 => July <br/>";
						break;
					case 8:
						echo "8 => August <br/>";
						break;
					case 9:
						echo "9 => September <br/>";
						break;
					case 10:
						echo "10 => Octomber <br/>";
						break;
					case 11:
						echo "11 => November <br/>";
						break;
					case 12:
						echo "12 => December <br/>";
						break;
					default:
						echo "Invalid input.<br/>";
						break;
			}
		}
		numbToMonth(8);
		numbToMonth(13);
	?>
</div>
</body>
</html>